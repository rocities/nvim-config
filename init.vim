" vim-plug
call plug#begin()

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
" NERD tree will be loaded on the first invocation of NERDTreeToggle command
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" visual
Plug 'itchyny/lightline.vim'
Plug 'arcticicestudio/nord-vim'

" experimental
Plug 'tpope/vim-sleuth'

call plug#end()


" general
set ignorecase
set smartcase
set tabstop=2
set shiftwidth=2
set expandtab
:set number relativenumber
:set nu rnu
set clipboard+=unnamedplus
" Use <C-I> to clear the highlighting of :set hlsearch.
if maparg('<C-I>', 'n') ==# ''
  nnoremap <silent> <C-I> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-I>
endif

" NERDTree 
map <C-n> :NERDTreeToggle<CR>

" Nord colour scheme
set termguicolors
colorscheme nord

" Easier split navigations 
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
